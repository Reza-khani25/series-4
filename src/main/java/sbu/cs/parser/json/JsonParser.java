package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class JsonParser {

    /*
    * this function will get a String and returns a Json object
     */
    public static Json parse(String data) {

        // TODO implement this

        data = data.replace(" " , "") ;
        List<String> keyValues = new ArrayList<>();

        String keyValueRegex = "(\"\\w+\":\\[(\\w,?)+\\])|(\"\\w+\":?(\\w+)?(\"\\w+\")?)";
        Matcher m = Pattern.compile(keyValueRegex).matcher(data) ;
        while(m.find())
        {
            keyValues.add(m.group()) ;
        }

        Json json = new Json();
        for(String str : keyValues)
        {
            String[] splitted = str.split(":");
            json.setInstance(splitted[0].replace("\"" , "") , splitted[1].replace("\"" , ""));
        }


        return json;
    }

    /*
    * this function is the opposite of above function. implementing this has no score and
    * is only for those who want to practice more.
     */
    public static String toJsonString(Json data) {
        return null;
    }


}
