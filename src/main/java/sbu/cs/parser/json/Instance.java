package sbu.cs.parser.json;

public class Instance {

    private String key ;
    private String value ;

    public String getValue(){
        return value ;
    }
    public String getKey() {
        return key;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Instance(String key , String value)
    {
        this.key = key ;
        this.value = value ;
    }


}
