package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.List;

public class Json implements JsonInterface {

   public List<Instance> instances = new ArrayList<>();

    public void setInstance(String key , String value)
    {
        Instance inst = new Instance(key , value) ;
        instances.add(inst);
    }


    @Override
    public String getStringValue(String key) {
        // TODO implement this

        for (Instance inst : instances)
        {
            if(inst.getKey().equals(key)) {

                if(inst.getValue().charAt(0) == '[')
                    inst.setValue(inst.getValue().replace(",", ", "));

                return inst.getValue();
            }
        }
        return null;
    }
}
