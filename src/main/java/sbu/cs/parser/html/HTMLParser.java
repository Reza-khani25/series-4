package sbu.cs.parser.html;
import java.util.ArrayList;
public class HTMLParser {
    public static Node parse(String document)
    {
        document = document.replace("\n", "");
        document = fixSpaceBug(document) ;
        // Node node = getNodeInside(document) ;
        Node node = new Node(document) ;

        node.setChildren(childrenBirth(document));


        return node;
    }

    public static Node getNodeInside(String document)  // avalin laye tag ra hazf mikonad
    {
        int firtIndex = document.indexOf('>') + 1;
        int lastIndex = document.lastIndexOf('<');

        document = document.substring(firtIndex, lastIndex) ;

        Node node = new Node(document);


        return node;
    }

    public static ArrayList<Node> childrenBirth(String document)
    {
        ArrayList<Node>children = new ArrayList<>();

        String context = getNodeInside(document).getContext() ;
        int endIndex = 0 ;

        for(int i = 0 ; i<context.length() ; i++)
        {


            int j ;
            if(context.charAt(i) == '<' && context.charAt(i+1) != '/'  && !isAttString(i, context) )
            {
                if(i<endIndex)
                    continue;

                //  System.out.println("i=" + i);

                for(j = i+1 ; j<context.length() ; j++)
                {
                    if(context.charAt(j) == '>' || context.charAt(j) == ' ')
                        break ;
                }
                String tag = context.substring(i+1, j ) ;
                String endOfTag = "</" + tag + ">" ;

                //  System.out.println("tag = " + tag + "   endoftag = " + endOfTag);
                int beginIndex = i  ;


                endIndex = context.substring(i).indexOf(endOfTag) + i + endOfTag.length();

                String child = context.substring(beginIndex , endIndex) ;
                //  System.out.println(child + "=child");
                Node temp = parse(child) ;
                children.add(temp) ;

            }

            if( context.charAt(i) == '<' && isAttString(i, context))
            {
                String child = null;
                Node temp = new Node(child) ;

                children.add(temp) ;

            }


        }



        return children ;
    }



    public static boolean isAttString(int i , String str)
    {
        int j = i;
        while(str.charAt(j) != '>')
        {
            if(str.charAt(j) == '=')
                return true;

            j++;

        }

        return false ;
    }

    public static String fixSpaceBug(String document)
    {
        String str = document;

        for(int i = 0 ; i<str.length() ; i++)
        {
            if(str.charAt(i) == '<' && str.charAt(i+1) != '/')
            {
                for(int j = i+1 ; j<str.length() ; j++)
                {
                    if(str.charAt(j) == '>')
                        break;

                    if(str.charAt(j) == ' ')
                    {
                        String temp = str.substring(0, j) ;
                        str = str.substring(j).replaceFirst(" ", "><") ;
                        str = temp + str ;
                        break;
                    }
                }
            }
        }

        return str;
    }

}



















//
//    /*
//    * a function that will return string representation of dom object.
//    * only implement this after all other functions been implemented because this
//    * impl is not required for this series of exercises. this is for more score
//     */
//    public static String toHTMLString(Node root) {
//        // TODO implement this for more score
//        return null;
//    }
//}
