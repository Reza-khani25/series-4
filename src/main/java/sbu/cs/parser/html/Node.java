package sbu.cs.parser.html;

import java.util.ArrayList;
import java.util.List;



public class Node implements NodeInterface {

    private String context ;
    private List<Node>children = new ArrayList<>() ;


    // setters
    public void setChildren(ArrayList<Node> children) {
        this.children = children;
    }


    // getters
    public String getContext() {
        return context;
    }



    // methodes

    @Override
    public String getStringInside() {

        if(this.context == null)
            return null;

        Node inside = HTMLParser.getNodeInside(this.context);

        return inside.getContext() ;
        // TODO implement this

    }

    /*
    *
     */
    @Override
    public List<Node> getChildren() {
        return children;
    }

    /*
    * in html tags all attributes are in key value shape. this function will get a attribute key
    * and return it's value as String.
    * for example <img src="img.png" width="400" height="500">
     */
    @Override
    public String getAttributeValue(String key) {
        // TODO implement this
        return null;
    }

    public Node(String context)
    {
        this.context = context ;
    }


}
